#!/bin/bash

sudo dnf install -y webp-pixbuf-loader -y #webp support

sudo dnf install -y solaar lutris
sudo dnf install -y libatomic #for discord

#microsoft fonts
sudo dnf install -y cabextract xorg-x11-font-utils fontconfigfont mscore-fonts-all
sudo rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm

sudo dnf install -y dconf-editor
sudo dnf install -y gnome-tweaks gnome-extensions-app