#!/bin/bash

sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

sudo dnf update	-y

sudo dnf groupinstall multimedia -y
sudo dnf group upgrade --with-optional Multimedia -y
sudo dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda vulkan nvidia-vaapi-driver vdpauinfo libva-vdpau-driver
sudo dnf install -y intel-media-driver libva-intel-driver libva libva-utils gstreamer1-vaapi ffmpeg intel-gpu-tools mpv libva-intel-hybrid-driver

# NVidia Suspend fix needed after 470 driver update
sudo dnf install -y xorg-x11-drv-nvidia-power
sudo systemctl enable nvidia-suspend
sudo systemctl enable nvidia-hibernate
sudo systemctl enable nvidia-resume
