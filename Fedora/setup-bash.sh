#!/bin/bash

# Menu Completion
echo "TAB: menu-complete" | sudo tee -a ~/.inputrc

# Powerline
sudo dnf install -y powerline

sudo echo "
# Powerline configuration
if [ -f /usr/share/powerline/bash/powerline.sh ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  source /usr/share/powerline/bash/powerline.sh
fi" >> ~/.bashrc

# Default Editor
sudo echo "export EDITOR='/usr/bin/nano'"  >> ~/.bashrc
