#!/bin/bash

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak install flathub org.remmina.Remmina -y
flatpak install flathub org.videolan.VLC -y
flatpak install flathub com.discordapp.Discord -y